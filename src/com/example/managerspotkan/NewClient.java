package com.example.managerspotkan;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class NewClient extends Activity {
	
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_client);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		context = getApplicationContext();
	}
	
	public void addClient(View button){
		
		EditText newClientName = (EditText)findViewById(R.id.new_client_name);
		String name = newClientName.getText().toString();
		EditText newClientCity = (EditText)findViewById(R.id.new_client_city);
		String city = newClientCity.getText().toString();
		EditText newClientCode = (EditText)findViewById(R.id.new_client_code);
		String code = newClientCode.getText().toString();
		EditText newClientStreet = (EditText)findViewById(R.id.new_client_street);
		String street = newClientStreet.getText().toString();
		EditText newClientNum = (EditText)findViewById(R.id.new_client_num);
		String num = newClientNum.getText().toString();
		
		DbHelper mDbHelper = new DbHelper(this);
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		
		if(name.length()==0){
			Toast.makeText(context, "Wpisz nazw� klienta", Toast.LENGTH_SHORT).show();
		}
		else if(city.length()==0 || code.length()==0 || street.length()==0 || num.length()==0){
			Toast.makeText(context, "Wpisz pe�en adres", Toast.LENGTH_SHORT).show();
		}
		if(code.length()!=6 /*doda� format*/){
			Toast.makeText(context, "Wpisz prawid�owy kod", Toast.LENGTH_SHORT).show();
		}
		
		else {
			ContentValues values = new ContentValues();
			//values.put(FeedEntry.CL_ID, 1);
			values.put(FeedEntry.CL_NAME, name);
			values.put(FeedEntry.CL_CITY, city);
			values.put(FeedEntry.CL_CODE, code);
			values.put(FeedEntry.CL_ST, street);
			values.put(FeedEntry.CL_NUM, num);
			db.insert(FeedEntry.CL_TABLE,null,values); 
			
			Toast.makeText(context, "Dodano nowego klienta", Toast.LENGTH_SHORT).show();
			Intent clientList = new Intent(this, ClientList.class);
			startActivity(clientList);
		}
	}
	
	public void hideKeyboard(View button){
		 
		InputMethodManager imm = (InputMethodManager)getSystemService(
		Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(button.getWindowToken(), 0);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
}
