package com.example.managerspotkan;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ClientView extends Activity {

	String id; 
	ClientView me;
	Context context;
	String address;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_client_view);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		Intent intent = getIntent();
		id = intent.getStringExtra("client_id");
		me=this;
		context = getApplicationContext();
		loadInfo();
		loadContacts();
	}
	
	void loadInfo(){
		TextView clientNameView = (TextView)findViewById(R.id.clview_name);
		TextView clientAddr1View = (TextView)findViewById(R.id.clview_addr1);
		TextView clientAddr2View = (TextView)findViewById(R.id.clview_addr2);
		
		DbHelper mDbHelper = new DbHelper(this);
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		String[] projection = {
				FeedEntry.CL_ID,
				FeedEntry.CL_NAME, 
				FeedEntry.CL_CITY, 
				FeedEntry.CL_CODE, 
				FeedEntry.CL_ST, 
				FeedEntry.CL_NUM
		};

		//String sortOrder = FeedEntry.CL_NAME+" DESC";
		Cursor cursor = db.query(
				FeedEntry.CL_TABLE,  // The table to query
			    projection,  // The columns to return
			    FeedEntry.CL_ID +" like "+id, 	// The columns for the WHERE clause
			    null,  // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		
		cursor.moveToFirst();
        String clientName = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_NAME));
		String clientCity = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_CITY));
		String clientCode = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_CODE));
		String clientStreet = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_ST));
		String clientNumber = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_NUM));
		address = clientCity+" "+clientCode+" "+clientStreet+" "+clientNumber;
		clientNameView.setText(clientName);
		clientAddr1View.setText(clientCity+" "+clientCode);
		clientAddr2View.setText(clientStreet+" "+clientNumber);
	}
	
	void loadContacts(){
		
		DbHelper mDbHelper = new DbHelper(this);
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		String[] projection = {FeedEntry.CON_ID, FeedEntry.CON_NAME, FeedEntry.CON_NUMB};

		String sortOrder = FeedEntry.CON_NAME;
		Cursor c = db.query(
				FeedEntry.CON_TABLE,  // The table to query
			    projection,  // The columns to return
			    FeedEntry.CON_COMP +" like "+id, 	// The columns for the WHERE clause
			    null,  // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    sortOrder                                 // The sort order
				);


		TableLayout table = (TableLayout)findViewById(R.id.contacts_table);
		if(c.moveToFirst()){
			do{
				String name = c.getString(c.getColumnIndexOrThrow(FeedEntry.CON_NAME));
				String phone = c.getString(c.getColumnIndexOrThrow(FeedEntry.CON_NUMB));
				TableRow row = new TableRow(this);
				TextView writeName = new TextView(this);
				writeName.setText(name+" ");
				TextView writePhone = new TextView(this);
				writePhone.setText(" "+phone);
				writePhone.setAutoLinkMask(Linkify.PHONE_NUMBERS);
				row.addView(writeName);
				row.addView(writePhone);
				table.addView(row);
			}while(c.moveToNext());
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	public void editClient(View button){
		Intent edit = new Intent(this, EditClient.class);
		edit.putExtra("client_id", ""+id);
		startActivity(edit);
	}
	
	public void addMeeting(View button){
		Intent meet = new Intent(this, NewMeeting.class);
		meet.putExtra("client_id", ""+id);
		startActivity(meet);
	}
	
	public void showMap(View button){
		String uri = "geo:0,0?q="+address; //test
		Intent map = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
		startActivity(map);
	}
	
	public void deleteClient(View button){
		
		new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Usun�� klienta?")
        .setMessage("Nieodwracalnie usuni�ty zostanie klient i wszystkie spotkania. Kontynuowa�?")
        .setPositiveButton("YOLO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
        		DbHelper mDbHelper = new DbHelper(me);
        		SQLiteDatabase db = mDbHelper.getReadableDatabase();
        		db.execSQL("DELETE FROM "+FeedEntry.CL_TABLE
        				+ " WHERE " + FeedEntry.CL_ID + " = " + id);
        		db.execSQL("DELETE FROM "+FeedEntry.MT_TABLE
        				+ " WHERE " + FeedEntry.MT_CL + " = " + id);
        		Intent back = new Intent(me, ClientList.class);
        		startActivity(back);
            }

        })
        .setNegativeButton("Nope", null)
        .show();
		
	}
}
