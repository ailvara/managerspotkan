package com.example.managerspotkan;

import android.provider.BaseColumns;

public final class FeedReaderContract {

    public FeedReaderContract() {}

    public static abstract class FeedEntry implements BaseColumns {
    	
		public static final String CL_TABLE = "clients";
		public static final String CL_ID ="_ID";
		public static final String CL_NAME ="cl_name";
		public static final String CL_CITY="cl_city";
		public static final String CL_CODE="cl_code";
		public static final String CL_ST="cl_street";
		public static final String CL_NUM="cl_number";
		
		public static final String MT_TABLE = "meetings";
		public static final String MT_ID = "mt_id";
		public static final String MT_CL = "mt_client";
		public static final String MT_CON = "mt_contact";
		public static final String MT_TIME = "mt_time";
		public static final String MT_DESCR = "mt_description";
		public static final String MT_ENDTIME = "mt_endtime";
		public static final String MT_LOCATION = "mt_location";
		
		public static final String CON_TABLE = "contacts";
		public static final String CON_ID = "ct_id";
		public static final String CON_COMP = "ct_company";
		public static final String CON_NAME = "ct_name";
		public static final String CON_NUMB = "ct_number";
		
		public static final String SQL_CREATE_CLIENTS =
				"CREATE TABLE " + FeedEntry.CL_TABLE + " (" +
				FeedEntry.CL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
				FeedEntry.CL_NAME + " TEXT, "+
				FeedEntry.CL_CITY + " TEXT, "+
				FeedEntry.CL_CODE + " TEXT, "+
				FeedEntry.CL_ST + " TEXT, "+
				FeedEntry.CL_NUM + " INTEGER)";
		
		public static final String SQL_CREATE_MEETINGS =
				"CREATE TABLE " + FeedEntry.MT_TABLE + " (" +
				FeedEntry.MT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
				FeedEntry.MT_CL + " INT, "+
				FeedEntry.MT_CON + " TEXT, "+
				FeedEntry.MT_TIME + " TEXT, "+
				FeedEntry.MT_DESCR + " TEXT, "+
				FeedEntry.MT_ENDTIME + " TEXT, "+
				FeedEntry.MT_LOCATION + " TEXT "+
				//"FOREIGN KEY("+FeedEntry.MT_ID+") REFERENCES "+FeedEntry.CL_TABLE+" ("+FeedEntry.CL_ID+")"+
		    " );";
		
		public static final String SQL_CREATE_CONTACTS =
				"CREATE TABLE " + FeedEntry.CON_TABLE + " (" +
				FeedEntry.CON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
				FeedEntry.CON_COMP + " INTEGER, "+
				FeedEntry.CON_NAME + " TEXT, "+
				FeedEntry.CON_NUMB + " TEXT ); ";
		
    }
		
}