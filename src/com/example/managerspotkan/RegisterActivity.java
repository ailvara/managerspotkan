package com.example.managerspotkan;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {
	
	Context context;
	SharedPreferences prefs;
	Resources r;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		context = getApplicationContext();
		prefs = getSharedPreferences("login_data", Context.MODE_PRIVATE);
		r = getResources();
	}
	
	public void register(View button){
		
		Context context = getApplicationContext();
		
		//Pobierz wpisane warto�ci z p�l tekstowych
		EditText textLogin = (EditText)findViewById(R.id.create_login);
		EditText textPass1 = (EditText)findViewById(R.id.create_password);
		EditText textPass2 = (EditText)findViewById(R.id.confirm_password);
		String inputLogin = textLogin.getText().toString();
		String inputPass1 = textPass1.getText().toString();
		String inputPass2 = textPass2.getText().toString();
		
		//Sprawd�, czy wpisano login
		if(inputLogin.length()==0) {
			Toast.makeText(context, r.getString(R.string.no_login), Toast.LENGTH_SHORT).show();
		}
		
		//Sprawd�, czy wpisano has�o
		else if(inputPass1.length()==0) {
			Toast.makeText(context, r.getString(R.string.no_password), Toast.LENGTH_SHORT).show();
		}
		
		//Sprawd�, czy poprawnie potwierdzono has�o
		else if(!inputPass1.equals(inputPass2)){
			Toast.makeText(context, r.getString(R.string.pass_mismatch), Toast.LENGTH_SHORT).show();
			textPass1.setText("");
			textPass2.setText("");
		}
		
		//Je�li wszystko ok, utw�rz konto
		else {
			SharedPreferences.Editor editor = prefs.edit();
			editor = editor.putString(r.getString(R.string.login_prefs), inputLogin);
			
			String hashPass="";
			//Zakoduj has�o
			
			try {
				//Kodowanie w MD5
				//http://howtodoinjava.com/2013/07/22/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(inputPass1.getBytes());
				byte[] bytes = md.digest();
	            //This bytes[] has bytes in decimal format; convert it to hexadecimal format
	            StringBuilder sb = new StringBuilder();
	            for(int i=0; i< bytes.length ;i++)
	            {
	                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	            //Get complete hashed password in hex format
	            hashPass = sb.toString();
				editor = editor.putString(r.getString(R.string.pass_prefs), hashPass);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			editor.commit();
			Toast.makeText(context, r.getString(R.string.register_success), Toast.LENGTH_SHORT).show();
			//Wr�� do ekranu logowania
			Intent login = new Intent(this, LoginActivity.class);
			startActivity(login);
		}
		
	}
	
	public void hideKeyboard(View button){
		 
		InputMethodManager imm = (InputMethodManager)getSystemService(
		Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(button.getWindowToken(), 0);
		
	}
}
