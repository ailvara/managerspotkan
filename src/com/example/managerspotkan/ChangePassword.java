package com.example.managerspotkan;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePassword extends Activity {
	
	Context context;
	SharedPreferences prefs;
	Resources r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		context = getApplicationContext();
		prefs = getSharedPreferences("login_data", Context.MODE_PRIVATE);
		r = getResources();
	}
	
	public void changePassword(View button){
		EditText oP = (EditText)findViewById(R.id.old_password);
		EditText nP = (EditText)findViewById(R.id.new_password);
		EditText npC = (EditText)findViewById(R.id.conf_new_password);
		String oldPass = oP.getText().toString();
		String newPass = nP.getText().toString();
		String confPass = npC.getText().toString();
		String savedPass = prefs.getString(r.getString(R.string.pass_prefs), "");
		
		if(oldPass.length()==0) {
			Toast.makeText(context, "Podaj stare has�o", Toast.LENGTH_SHORT).show();
		}
		
		//Sprawd�, czy wpisano has�o
		else if(newPass.length()==0) {
			Toast.makeText(context, "Podaj nowe has�o", Toast.LENGTH_SHORT).show();
		}
		
		//Sprawd�, czy poprawnie potwierdzono has�o
		else if(!newPass.equals(confPass)){
			Toast.makeText(context, r.getString(R.string.pass_mismatch), Toast.LENGTH_SHORT).show();
			nP.setText("");
			npC.setText("");
		}
		
		else {
			
			try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(oldPass.getBytes());
				byte[] bytes = md.digest();
	            //This bytes[] has bytes in decimal format;
	            //Convert it to hexadecimal format
	            StringBuilder sb = new StringBuilder();
	            for(int i=0; i< bytes.length ;i++)
	            {
	                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	            //Get complete hashed password in hex format
	            String hashPass = sb.toString();
				//Toast.makeText(context, hashPass, Toast.LENGTH_SHORT).show();
				if(!savedPass.equals(hashPass)){
					Toast.makeText(context, "Niepoprawne stare has�o", Toast.LENGTH_SHORT).show();
				}
				else {
					md.update(newPass.getBytes());
					bytes = md.digest();
		            //This bytes[] has bytes in decimal format;
		            //Convert it to hexadecimal format
		            sb = new StringBuilder();
		            for(int i=0; i< bytes.length ;i++)
		            {
		                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		            }
		            //Get complete hashed password in hex format
		            hashPass = sb.toString();
		            SharedPreferences.Editor editor = prefs.edit();
		            editor = editor.putString(r.getString(R.string.pass_prefs), hashPass);
		            editor.commit();
					Toast.makeText(context, "Has�o zosta�o zmienione", Toast.LENGTH_SHORT).show();
					Intent back = new Intent(this, MenuActivity.class);
					startActivity(back);
				}
				
			} catch(NoSuchAlgorithmException e) {}
		}
	}
	
	public void hideKeyboard(View button){
		 
		InputMethodManager imm = (InputMethodManager)getSystemService(
		Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(button.getWindowToken(), 0);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
