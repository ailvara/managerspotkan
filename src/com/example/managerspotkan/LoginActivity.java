package com.example.managerspotkan;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	Context context;
	SharedPreferences prefs;
	Resources r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		context = getApplicationContext();
		prefs = getSharedPreferences("login_data", Context.MODE_PRIVATE);
		r = getResources();
		if(prefs.getString(r.getString(R.string.login_prefs), "").equals("")) {
			Intent register = new Intent(this, RegisterActivity.class);
			startActivity(register);
		}
		//TEST
		Intent menu = new Intent(this, MenuActivity.class);
		startActivity(menu);
	}
	
	public void login(View button){
		EditText textLogin = (EditText)findViewById(R.id.login);
		EditText textPass = (EditText)findViewById(R.id.password);
		String inputLogin = textLogin.getText().toString();
		String inputPass = textPass.getText().toString();
		String savedLogin = prefs.getString(r.getString(R.string.login_prefs), "");
		String savedPass = prefs.getString(r.getString(R.string.pass_prefs), "");
		//http://howtodoinjava.com/2013/07/22/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(inputPass.getBytes());
			byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            String hashPass = sb.toString();
			//Toast.makeText(context, hashPass, Toast.LENGTH_SHORT).show();
			if(savedLogin.equals(inputLogin) && savedPass.equals(hashPass)){
				Toast.makeText(context, "Zalogowano.", Toast.LENGTH_SHORT).show();
				Intent menu = new Intent(this, MenuActivity.class);
				startActivity(menu);
			}
			else {
				Toast.makeText(context, getResources().getString(R.string.login_fail), Toast.LENGTH_SHORT).show();
				textLogin.setText("");
				textPass.setText("");
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	public void clear(View button){
		SharedPreferences.Editor editor = prefs.edit();
		editor.remove("login");
		editor.commit();
		Intent login = new Intent(this, LoginActivity.class);
		startActivity(login);
	}
	
	public void hideKeyboard(View button){
		 
		InputMethodManager imm = (InputMethodManager)getSystemService(
		Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(button.getWindowToken(), 0);
		
	}
	
	
}
