package com.example.managerspotkan;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

@SuppressLint("ValidFragment")
public class EndMeeting extends DialogFragment implements LocationListener {
	
	Context context;
	View view;
	boolean unfold;
	SimpleDateFormat f;
	TextView nowTime;
	int meeting;
	
	LocationManager locationManager;
	LocationListener locationListener;
	
	public EndMeeting(){
		super();
	}
	
	public EndMeeting(Context context){
		this();
		this.context = context;
	}
		
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
    	
        Builder builder = new Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        Bundle bundle = getArguments();
        meeting = bundle.getInt("meeting_id");

        unfold = false;
        
        final Date date = new Date();
        f = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        final String now = f.format(date);

        view = inflater.inflate(R.layout.end_meeting, null);
        
        //Lokalizacja
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        
        nowTime = (TextView)view.findViewById(R.id.now);
        nowTime.setText(now);
        
        Button changeTime = (Button)view.findViewById(R.id.change_time);
        changeTime.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
        		DatePicker date = (DatePicker)view.findViewById(R.id.change_date);
            	TimePicker time = (TimePicker)view.findViewById(R.id.change_hour);
            	if(unfold==false){
                	date.setVisibility(View.VISIBLE);
                	time.setVisibility(View.VISIBLE);
                	Button b = (Button)v;
                	b.setText("Potwierd�");
            		unfold=true;
            	}
            	else{
                	date.setVisibility(View.GONE);
                	time.setVisibility(View.GONE);
                	NumberFormat f = DecimalFormat.getInstance();
            		f.setMinimumIntegerDigits(2);
            		int year = date.getYear();
            		int month = date.getMonth()+1;
            		int day = date.getDayOfMonth();
            		int hour = time.getCurrentHour();
            		int minute = time.getCurrentMinute();
            		String newTime = ""+year+"-"+f.format(month)+"-"+f.format(day)+" "+f.format(hour)+":"+f.format(minute);
                	nowTime.setText(newTime);
            		Button b = (Button)v;
                	b.setText("Zmie� czas");
            		unfold=false;
            	}
            	
            }

        });
        
        builder.setView(view)
        		
        		.setTitle("Zako�czy� spotkanie?")
        
               .setPositiveButton("Potwierd�", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {	
                	    Toast.makeText(context, "Dzialam", Toast.LENGTH_SHORT).show();
                   		DbHelper dbHelper = new DbHelper(context);
                   		SQLiteDatabase db = dbHelper.getWritableDatabase();
                   		TextView endtime = (TextView)view.findViewById(R.id.now);
                   		String saveTime = endtime.getText().toString();
                   				db.execSQL("UPDATE "+FeedEntry.MT_TABLE
                   						+ " SET " + FeedEntry.MT_ENDTIME + " = \"" + saveTime
                   						+ "\" WHERE " + FeedEntry.MT_ID + " = " + meeting);

                        Toast.makeText(context, "Dzialam2", Toast.LENGTH_SHORT).show();
                   		Intent meeting = new Intent(context, MeetingView.class);
                   		meeting.putExtra("meet_id", ""+id);
                   		startActivity(meeting);
                   		}
                   
               })
               .setNegativeButton("Powr�t", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                   }
               });
        
        // Create the AlertDialog object and return it
        return builder.create();
    }

	@Override
	public void onLocationChanged(Location location) {
        TextView loc = (TextView)view.findViewById(R.id.location);
        loc.setText("Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());
		Toast.makeText(context, ""+location.getLongitude(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
	    
}