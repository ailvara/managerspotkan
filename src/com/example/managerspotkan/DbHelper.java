package com.example.managerspotkan;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {


	public static final String DATABASE_NAME = "database.db";
    public static final int DB_VERSION = 4;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FeedEntry.SQL_CREATE_CLIENTS);
        db.execSQL(FeedEntry.SQL_CREATE_MEETINGS);
        db.execSQL(FeedEntry.SQL_CREATE_CONTACTS);
    	Log.d("FeedReaderDbHelper", "Table clients created.");
    }
    
	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
	}
	
}