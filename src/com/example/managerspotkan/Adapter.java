package com.example.managerspotkan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map.Entry;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

public class Adapter extends BaseExpandableListAdapter {
	
	Context context;
	Cursor c;
	ExpandableListView list;
	ArrayList<String> days;
	HashMap<String,ArrayList<String[]>> dayMeets;
	HashMap<Integer,Integer> idsPos;
	boolean today;
	
	Adapter(Context con, Cursor cur, ExpandableListView el){
		context = con;
		c = cur;
		list = el;
		dayMeets = new HashMap<String, ArrayList<String[]>>();
		idsPos = new HashMap<Integer,Integer>();
		days = new ArrayList<String>();
		prepareData();
		today=false;
	}
	
	public void prepareData(){
		if(c.moveToFirst()){
			do {
				String date = c.getString(c.getColumnIndexOrThrow(FeedEntry.MT_TIME));
				date = date.substring(0,10);
				String m_id = c.getString(c.getColumnIndexOrThrow(FeedEntry.MT_ID));
				String descr = c.getString(c.getColumnIndexOrThrow(FeedEntry.MT_DESCR));
				String time = c.getString(c.getColumnIndexOrThrow(FeedEntry.MT_TIME)).substring(10,16);
				int cl_id = c.getInt(c.getColumnIndexOrThrow(FeedEntry.MT_CL));
				String[] client = getClientData(cl_id);
				String cl_name = client[0];
				String cl_address = client[1];
				String[] data = {m_id, time, cl_name, cl_address, descr};
				if(dayMeets.get(date)==null){
					days.add(date);
					ArrayList<String[]> meets = new ArrayList<String[]>();
					meets.add(data);
					dayMeets.put(date, meets);
				}
				else {
					dayMeets.get(date).add(data);
				}
				
			} while(c.moveToNext());
			
			Collections.sort(days);
			for(Entry<String,ArrayList<String[]>> pair : dayMeets.entrySet()){
				ArrayList<String[]> list = pair.getValue();
			}
			
		}
	}
	
	String[] getClientData(int id){
		DbHelper mDbHelper = new DbHelper(context);
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		String[] projection = {
				FeedEntry.CL_ID,
				FeedEntry.CL_NAME, 
				FeedEntry.CL_CITY, 
				FeedEntry.CL_CODE, 
				FeedEntry.CL_ST, 
				FeedEntry.CL_NUM
		};

		Cursor cursor = db.query(
				FeedEntry.CL_TABLE,  // The table to query
			    projection,  // The columns to return
			    FeedEntry.CL_ID +" like "+id, 	// The columns for the WHERE clause
			    null,  // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		
		cursor.moveToFirst();
        String clientName = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_NAME));
		String clientCity = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_CITY));
		String clientCode = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_CODE));
		String clientStreet = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_ST));
		String clientNumber = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_NUM));
		
		String address = clientCity+" "+clientCode+" "+clientStreet+" "+clientNumber;
		String[] info = {clientName, address};
		return info;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return (long)idsPos.get(childPosition);
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inf.inflate(R.layout.date_meet, null);
		TextView date = (TextView) convertView.findViewById(R.id.meet_date);
		TextView client = (TextView) convertView.findViewById(R.id.meet_client);
		TextView id = (TextView) convertView.findViewById(R.id.meet_id);
		TextView address = (TextView) convertView.findViewById(R.id.meet_address);
		//TextView descr = (TextView) convertView.findViewById(R.id.meet_descr);
		String group = days.get(groupPosition);
		String[] info = dayMeets.get(group).get(childPosition);
		date.setText(info[1]);
		client.setText(info[2]);
		address.setText(info[3]);
		//descr.setText(info[4]);
		id.setText("["+info[0]+"]");
		int m_id = Integer.parseInt(info[0]);
		idsPos.put(childPosition, m_id);
		//Toast.makeText(context, idsPos.get(childPosition), Toast.LENGTH_SHORT).show();
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		String date = days.get(groupPosition);
		int size = dayMeets.get(date).size();
		return size;
	} 

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		int count = days.size();
		return count;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inf.inflate(R.layout.date_group, null);
		String dateGroup = (String) days.get(groupPosition);
		TextView title = (TextView) convertView.findViewById(R.id.date_group_title);
				
		final Date date = new Date();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    final String strDate = sdf.format(date);
	    
	    if(strDate.equals(dateGroup) && today==false){
			list.expandGroup(groupPosition);
			today=true;
	    }
	    
		title.setText(dateGroup);
		
		return convertView;

	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	
}