package com.example.managerspotkan;

import java.util.HashMap;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ClientList extends ListActivity {
	
	Context context;
	SQLiteDatabase db;
	ListView l;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_client_list);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		context = getApplicationContext();
		createDatabase();
	    //addExampleToBase();
		l =  getListView();
		ClientListAdapter adapter=new ClientListAdapter(context, loadFromDatabase(), l);
	    setListAdapter(adapter);
	}
	
	 @Override
     protected void onListItemClick(ListView l, View v, int position, long id) {
		 super.onListItemClick(l, v, position, id);
		 Intent client = new Intent(this, ClientView.class);
		 client.putExtra("client_id", ""+id);
		 startActivity(client);
     }
	
	public void addClient(View button){
		Intent new_client = new Intent(this, NewClient.class);
		startActivity(new_client);
	}
	
	Cursor loadFromDatabase(){
		
		String[] projection = {
				FeedEntry.CL_ID,
				FeedEntry.CL_NAME, 
				FeedEntry.CL_CITY, 
				FeedEntry.CL_CODE, 
				FeedEntry.CL_ST, 
				FeedEntry.CL_NUM
		};

		//String sortOrder = FeedEntry.CL_NAME+" DESC";
		Cursor c = db.query(
				FeedEntry.CL_TABLE,  // The table to query
			    projection,                               // The columns to return
			    null,                                // The columns for the WHERE clause
			    null,                            // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		return c;
	}
	
	void createDatabase(){
		DbHelper mDbHelper = new DbHelper(this);
		try {
	        db = mDbHelper.getWritableDatabase();
	    } catch (SQLException e) {
	        db = mDbHelper.getReadableDatabase();
	    }
	}
	
	void addExampleToBase(){
		ContentValues values = new ContentValues();
		values.put(FeedEntry.CL_NAME, "Przyk�adowa nazwa");
		values.put(FeedEntry.CL_CITY, "Gdynia");
		values.put(FeedEntry.CL_CODE, "11-111");
		values.put(FeedEntry.CL_ST, "Jaka�");
		values.put(FeedEntry.CL_NUM, "111/2");
		db.insert(FeedEntry.CL_TABLE,null,values);
		values = new ContentValues();
		values.put(FeedEntry.CL_NAME, "Jaka� firma");
		values.put(FeedEntry.CL_CITY, "Gda�sk");
		values.put(FeedEntry.CL_CODE, "22-222");
		values.put(FeedEntry.CL_ST, "Ulica");
		values.put(FeedEntry.CL_NUM, "13");
		db.insert(FeedEntry.CL_TABLE,null,values);
		values = new ContentValues();
		values.put(FeedEntry.CL_NAME, "Inna firma");
		values.put(FeedEntry.CL_CITY, "Sopot");
		values.put(FeedEntry.CL_CODE, "33-333");
		values.put(FeedEntry.CL_ST, "Street");
		values.put(FeedEntry.CL_NUM, "2");
		db.insert(FeedEntry.CL_TABLE,null,values);
	}
	
	class ClientListAdapter extends BaseAdapter {
		Context context;
		Cursor cursor;
		ListView l;
		HashMap<Integer,Integer> ids = new HashMap<Integer, Integer>();
		
		ClientListAdapter(Context con, Cursor cur, ListView listView){
			super();
			context=con;
			cursor=cur;
			l = listView;
		}

		@Override
		public int getCount() {
			return cursor.getCount();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return (long)ids.get(position);
		}

		@Override
		public View getView(int position, View view, ViewGroup parent){
			// inflate the layout for each item of listView
			//http://www.learn-android-easily.com/2013/06/listview-with-custom-adapter.html
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        view = inflater.inflate(R.layout.listview_each_item, null);
	        cursor.moveToPosition(position);
	        int id = cursor.getInt(cursor.getColumnIndexOrThrow("_ID"));
	        String clientName = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_NAME));
	        TextView clName=(TextView)view.findViewById(R.id.list_clname);
	        clName.setText(clientName);
	        ids.put(position,id);
	        return view;
	    }
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}