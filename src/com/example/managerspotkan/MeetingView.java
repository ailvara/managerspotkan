package com.example.managerspotkan;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MeetingView extends Activity {

	String id; 
	MeetingView me;
	Context context;
	String address;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_meeting_view);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		Intent intent = getIntent();
		id = intent.getStringExtra("meet_id");
		context = getApplicationContext();
		me=this;
		loadInfo();
	}
	
	void loadInfo(){
		TextView timeView = (TextView)findViewById(R.id.meeting_time);
		TextView clientView = (TextView)findViewById(R.id.meeting_client);
		TextView contactName = (TextView)findViewById(R.id.contact_name);
		TextView contactPhone = (TextView)findViewById(R.id.call_contact);
		TextView addressView = (TextView)findViewById(R.id.meeting_address);
		TextView descrView = (TextView)findViewById(R.id.meeting_descr);
		
		DbHelper mDbHelper = new DbHelper(this);
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		String[] projection1 = {
				FeedEntry.MT_ID,
				FeedEntry.MT_CL, 
				FeedEntry.MT_CON, 
				FeedEntry.MT_DESCR, 
				FeedEntry.MT_TIME,
				FeedEntry.MT_ENDTIME, 
				FeedEntry.MT_LOCATION,
				
		};
		
		String[] projection2 = {
				FeedEntry.CL_ID,
				FeedEntry.CL_NAME, 
				FeedEntry.CL_CITY, 
				FeedEntry.CL_ST,
				FeedEntry.CL_CODE, 
				FeedEntry.CL_NUM,
		};
		
		String[] projection3 = {
				FeedEntry.CON_ID,
				FeedEntry.CON_NAME, 
				FeedEntry.CON_NUMB
		};

		Cursor cursor = db.query(FeedEntry.MT_TABLE,projection1,FeedEntry.MT_ID +" like "+id,null,null,null,null);
		
		cursor.moveToFirst();
		int cl_id = cursor.getInt(cursor.getColumnIndexOrThrow(FeedEntry.MT_CL));
		
		Cursor c = db.query(
				FeedEntry.CL_TABLE,  // The table to query
			    projection2,  // The columns to return
			    FeedEntry.CL_ID +" like "+cl_id, 	// The columns for the WHERE clause
			    null,  // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		
		c.moveToFirst();
        String time = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.MT_TIME));
		String client = c.getString(c.getColumnIndexOrThrow(FeedEntry.CL_NAME));
		String city = c.getString(c.getColumnIndexOrThrow(FeedEntry.CL_CITY));
		String code = c.getString(c.getColumnIndexOrThrow(FeedEntry.CL_CODE));
		String street = c.getString(c.getColumnIndexOrThrow(FeedEntry.CL_ST));
		String num = c.getString(c.getColumnIndexOrThrow(FeedEntry.CL_NUM));
		int contact_id = cursor.getInt(cursor.getColumnIndexOrThrow(FeedEntry.MT_CON));
		address = city+" "+code+" "+street+" "+num;
		
		Cursor c3 = db.query(
				FeedEntry.CON_TABLE,  // The table to query
			    projection3,  // The columns to return
			    FeedEntry.CON_ID +" = "+contact_id, 	// The columns for the WHERE clause
			    null,  // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		
		//Toast.makeText(context, ""+contact_id, Toast.LENGTH_SHORT).show();
		
		c3.moveToFirst();
		String contact = c3.getString(c3.getColumnIndexOrThrow(FeedEntry.CON_NAME));
		String phone = c3.getString(c3.getColumnIndexOrThrow(FeedEntry.CON_NUMB));
		String descr = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.MT_DESCR));
		String endtime = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.MT_ENDTIME));
		String location = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.MT_LOCATION));
		
		if(endtime!=null){
			TextView end = (TextView)findViewById(R.id.meeting_end);
			end.setText(endtime+"\n"+location);
			Button endButton = (Button)findViewById(R.id.end_meeting);
			//endButton.setClickable(false);
			endButton.setBackgroundColor(Color.parseColor("#000000"));
		}
		
		timeView.setText(time);
		clientView.setText(client);
		contactName.setText(contact);
		contactPhone.setText(phone);
		addressView.setText(address);
		descrView.setText(descr);

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	public void editMeeting(View button){
		Intent edit = new Intent(this, EditMeeting.class);
		int meet = Integer.parseInt(id);
		edit.putExtra("meeting_id", meet);
		startActivity(edit);
	}

	public void endMeeting(View button){
		
		EndMeeting contact = new EndMeeting(context);
        Bundle ids = new Bundle();
        ids.putString("meeting_id", id);
        contact.setArguments(ids);
	    contact.show(getFragmentManager(), "tag");
	    
        /*final Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        final String strDate = sdf.format(date);
		new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_input_delete)
        .setTitle("Zako�cz spotkanie")
        .setMessage("Obecny czas: " + strDate + ". Ustawi� czas zako�czenia?")
        .setNegativeButton("Wr��", null)
        .setPositiveButton("zako�cz spotkanie", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
        		DbHelper mDbHelper = new DbHelper(me);
        		SQLiteDatabase db = mDbHelper.getReadableDatabase();
        		db.execSQL("UPDATE "+FeedEntry.MT_TABLE
        				+ " SET " + FeedEntry.MT_ENDTIME + " = \"" + strDate
        				+ "\" WHERE " + FeedEntry.MT_ID + " = " + id);
        		//Intent back = new Intent(me, ClientList.class);
        		//startActivity(back);
            }

        })
        .show();*/
	}
	
	public void showMap(View button){
		String loc = address.replaceAll(" ", "+");
		String uri = "geo:0,0?q="+loc; //test
		Intent map = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
		startActivity(map);
	}
	
	public void deleteMeeting(View button){
		
		new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Usun�� spotkanie?")
        .setMessage("Nieodwracalnie usuni�te zostan� wszystkie dane o spotkaniu. Kontynuowa�?")
        .setNegativeButton("Wr��", null)
        .setPositiveButton("USU�", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
        		DbHelper mDbHelper = new DbHelper(me);
        		SQLiteDatabase db = mDbHelper.getReadableDatabase();
        		db.execSQL("DELETE FROM "+FeedEntry.MT_TABLE
        				+ " WHERE " + FeedEntry.MT_ID + " = " + id);
        		Intent back = new Intent(me, MeetingsList.class);
        		startActivity(back);
            }

        })
        .show();
	
	}
	
}
