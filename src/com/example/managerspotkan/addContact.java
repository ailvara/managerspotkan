package com.example.managerspotkan;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

@SuppressLint("ValidFragment")
public class addContact extends DialogFragment{
	
	Context context;
	
	public addContact(){
		super();
	}
	
	public addContact(Context context){
		this();
		this.context = context;
	}
		
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
    	
        Builder builder = new Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        Bundle bundle = getArguments();
        final int company = bundle.getInt("companyName");
        View view = inflater.inflate(R.layout.add_contact, null);
        final EditText name = (EditText) view.findViewById(R.id.new_contact_name);
        final EditText number = (EditText) view.findViewById(R.id.new_contact_number);
        builder.setView(view)
        
               .setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   String newName = name.getText().toString();
                	   String newNumber = number.getText().toString();
                	   putToBase(company, newName, newNumber);
                       NewMeeting activity = (NewMeeting) getActivity();
                       activity.updateSpinner();
                   }
               })
               .setNegativeButton("Powr�t", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // User cancelled the dialog
                   }
               });
        // Create the AlertDialog object and return it
        return builder.create();
    }
    
    public void putToBase(int id, String name, String number){
    	DbHelper dbHelper = new DbHelper(context);
		SQLiteDatabase db = dbHelper.getWritableDatabase();
    	ContentValues values = new ContentValues();
		values.put(FeedEntry.CON_COMP, id);
		values.put(FeedEntry.CON_NAME, name);
		values.put(FeedEntry.CON_NUMB, number);
		db.insert(FeedEntry.CON_TABLE,null,values);
    }
    
}