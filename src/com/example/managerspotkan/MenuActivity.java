package com.example.managerspotkan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
	}
	
	public void launchClientList(View button){
		Intent clients = new Intent(this,ClientList.class);
		startActivity(clients);
	}
	
	public void newMeeting(View button){
		Intent meet = new Intent(this, NewMeeting.class);
		startActivity(meet);
	}
	
	public void logOut(View button){
		Intent login = new Intent(this, LoginActivity.class);
		Toast.makeText(this, "Wylogowano", Toast.LENGTH_LONG).show();
		startActivity(login);
	}
	
	public void meetingsList(View button){
		Intent mList = new Intent(this, MeetingsList.class);
		startActivity(mList);
	}
	
	public void changePassword(View button){
		Intent change = new Intent(this, ChangePassword.class);
		startActivity(change);
	}
}
