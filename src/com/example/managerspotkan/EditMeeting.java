package com.example.managerspotkan;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class EditMeeting extends Activity {
	
	int id;
	DbHelper dbHelper;
	SQLiteDatabase db;
	Context context;
	HashMap<Integer, Integer> conIds;
	ArrayAdapter<String> con_adapter;
	List<String> contactsList;
	String chosenContact;

	Spinner chooseContact;
	TextView clientView;
	EditText description;
	EditText timeView;
	
	int client;
	int contact;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_meeting);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		Intent intent = getIntent();
		id = intent.getIntExtra("meeting_id", 0);
		context = getApplicationContext();

		clientView = (TextView)findViewById(R.id.chosen_client);
		chooseContact = (Spinner)findViewById(R.id.edit_choose_contact);
		description = (EditText)findViewById(R.id.edit_description);
		timeView = (EditText)findViewById(R.id.edit_time);
		
		dbHelper = new DbHelper(context);
		db = dbHelper.getWritableDatabase();
		Toast.makeText(context, ""+id, Toast.LENGTH_SHORT).show();
		loadMeeting(id);
		
		conIds = new HashMap<Integer, Integer>();
		
		contactsList = loadContacts(client);
				
		con_adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, contactsList);
		chooseContact.setAdapter(con_adapter);
		
		Toast.makeText(context, chosenContact, Toast.LENGTH_SHORT).show();
		int previousContact = con_adapter.getPosition(chosenContact);
		chooseContact.setSelection(previousContact);
		
	}
	
	void loadMeeting(int meeting_id){
		String[] projection1 = {
				FeedEntry.MT_ID,
				FeedEntry.MT_CL, 
				FeedEntry.MT_CON, 
				FeedEntry.MT_DESCR, 
				FeedEntry.MT_TIME,
				FeedEntry.MT_ENDTIME, 
				FeedEntry.MT_LOCATION,
				
		};

		Cursor c = db.query(FeedEntry.MT_TABLE,projection1,FeedEntry.MT_ID+" like "+meeting_id,null,null,null,null);
		
		c.moveToFirst();
		client = c.getInt(c.getColumnIndexOrThrow(FeedEntry.MT_CL));
		contact = c.getInt(c.getColumnIndexOrThrow(FeedEntry.MT_CON));
		description.setText(c.getString(c.getColumnIndexOrThrow(FeedEntry.MT_DESCR)));
		timeView.setText(c.getString(c.getColumnIndexOrThrow(FeedEntry.MT_TIME)));
		
		//Nazwa klienta
		String[] projection2 = {FeedEntry.CL_NAME,FeedEntry.CL_ID};
		Cursor c2 = db.query(FeedEntry.CL_TABLE,projection2,FeedEntry.CL_ID+" like "+client,null,null,null,null);
		c2.moveToFirst();
		String clientName = c2.getString(c2.getColumnIndexOrThrow(FeedEntry.CL_NAME));
		clientView.setText(clientName);
		
	}
	
	ArrayList<String> loadContacts(int company){
		
		ArrayList<String> contacts = new ArrayList<String>();
		
		String[] projection = {
				FeedEntry.CON_ID,
				FeedEntry.CON_NAME,
				FeedEntry.CON_NUMB
		};

		//String sortOrder = FeedEntry.CON_TABLE;
		Cursor c = db.query(
				FeedEntry.CON_TABLE,  // The table to query
			    projection,                               // The columns to return
			    FeedEntry.CON_COMP +" like "+company,                                // The columns for the WHERE clause
			    null,                            // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		
		int i=0;
		if(c.moveToNext()) do {
			int con_id = c.getInt(c.getColumnIndexOrThrow(FeedEntry.CON_ID));
			String name = c.getString(c.getColumnIndexOrThrow(FeedEntry.CON_NAME));
			String numb = c.getString(c.getColumnIndexOrThrow(FeedEntry.CON_NUMB));
			contacts.add(name+" "+numb);
			conIds.put(i, con_id);
			i++;
			Toast.makeText(context, con_id+" "+contact, Toast.LENGTH_SHORT).show();
			if(con_id==contact) chosenContact=name+" "+numb;
		} while(c.moveToNext()); 
		
		return contacts;
	}
	
	public void editMeeting(View button){
		
		String time=timeView.getText().toString();
		String descr=description.getText().toString();

		int contact = chooseContact.getSelectedItemPosition();
		contact = conIds.get(contact);
		
		db.execSQL("UPDATE "+FeedEntry.MT_TABLE+" SET "
				+ FeedEntry.MT_CON + " = '" + contact + "', "
				+ FeedEntry.MT_DESCR + " = '" + descr + "', "
				+ FeedEntry.MT_TIME + " = '" + time + "'"
				+ " WHERE " + FeedEntry.MT_ID + " = " + id);
		
		Toast.makeText(context, "Zapisano zmiany", Toast.LENGTH_SHORT).show();
		
		Intent meeting = new Intent(this, MeetingView.class);
		meeting.putExtra("meet_id", ""+id);
		startActivity(meeting);
	}
	
	public void updateSpinner(){
		List<String> contactsList = loadContacts(client);
		chooseContact = (Spinner)findViewById(R.id.edit_choose_contact);
		ArrayAdapter<String> con_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, contactsList);
		chooseContact.setAdapter(con_adapter);
	}
	
	public void addContact(View button){
        addContact contact = new addContact(context);
        Bundle ids = new Bundle();
        ids.putInt("companyName", client);
        contact.setArguments(ids);
	    contact.show(getFragmentManager(), "tag");
	    updateSpinner();
	}
	
	public void hideKeyboard(View button){
		 
		InputMethodManager imm = (InputMethodManager)getSystemService(
		Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(button.getWindowToken(), 0);
		
	}
}