package com.example.managerspotkan;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

public class NewMeeting extends Activity {
	
	String client_id;
	Context context;
	DbHelper dbHelper;
	SQLiteDatabase db;
	HashMap<Integer, String> idsNames;
	HashMap<Integer, Integer> posIds;
	HashMap<Integer, Integer> conIds;
	Spinner chooseClient;
	Spinner chooseContact;
	ArrayAdapter<String> con_adapter;
	List<String> contactsList;
	EditText search;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_meeting);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		context = getApplicationContext();
		Intent intent = getIntent();
		client_id = intent.getStringExtra("client_id");
		
		dbHelper = new DbHelper(context);
		db = dbHelper.getWritableDatabase();
		idsNames = new HashMap<Integer, String>();
		posIds = new HashMap<Integer, Integer>();
		conIds = new HashMap<Integer, Integer>();
		List<String> clientList = loadClients();
		contactsList = loadContacts(client_id);
		
		chooseClient = (Spinner)findViewById(R.id.choose_client);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, clientList);
		chooseClient.setAdapter(adapter);
		
		chooseContact = (Spinner)findViewById(R.id.choose_contact);
		con_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, contactsList);
		chooseContact.setAdapter(con_adapter);
		
		chooseClient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

	        @Override
	        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	        	updateSpinner();
	        }

	        @Override
	        public void onNothingSelected(AdapterView<?> arg0) {
	        }       

	    });
		
		/*search = (EditText)findViewById(R.id.search_meetings);
		
		search.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				Toast.makeText(context, search.getText(), Toast.LENGTH_SHORT).show();
			}
            
		});*/
		
		if(client_id!=null) {
			String name = idsNames.get(Integer.parseInt(client_id));
			int spinnerPosition = adapter.getPosition(name);
			chooseClient.setSelection(spinnerPosition);
			//Toast.makeText(context, ""+spinnerPosition, Toast.LENGTH_SHORT).show();
		}

	}
	
	ArrayList<String> loadClients(){

		ArrayList<String> clientList = new ArrayList<String>();
		
		String[] projection = {
				FeedEntry.CL_ID,
				FeedEntry.CL_NAME
		};

		String sortOrder = FeedEntry.CL_NAME;
		Cursor c = db.query(
				FeedEntry.CL_TABLE,  // The table to query
			    projection,                               // The columns to return
			    null,                                // The columns for the WHERE clause
			    null,                            // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    sortOrder                                 // The sort order
				);
		
		int i=0;
		if(c.moveToNext()) do {
			int clientID = Integer.parseInt(c.getString(c.getColumnIndexOrThrow(FeedEntry.CL_ID)));
			String clientName = c.getString(c.getColumnIndexOrThrow(FeedEntry.CL_NAME));
			idsNames.put(clientID, clientName);
			clientList.add(clientName);
			posIds.put(i, clientID);
			i++;
		} while(c.moveToNext()); 
		
		return clientList;
	}
	
	ArrayList<String> loadContacts(String company){
		
		ArrayList<String> contacts = new ArrayList<String>();
		
		String[] projection = {
				FeedEntry.CON_ID,
				FeedEntry.CON_NAME,
				FeedEntry.CON_NUMB
		};

		//String sortOrder = FeedEntry.CON_TABLE;
		Cursor c = db.query(
				FeedEntry.CON_TABLE,  // The table to query
			    projection,                               // The columns to return
			    FeedEntry.CON_COMP +" like "+company,                                // The columns for the WHERE clause
			    null,                            // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		
		int i=0;
		if(c.moveToNext()) do {
			int con_id = c.getInt(c.getColumnIndexOrThrow(FeedEntry.CON_ID));
			String name = c.getString(c.getColumnIndexOrThrow(FeedEntry.CON_NAME));
			String numb = c.getString(c.getColumnIndexOrThrow(FeedEntry.CON_NUMB));
			contacts.add(""+con_id+" "+name+" "+numb);
			conIds.put(i, con_id);
			i++;
		} while(c.moveToNext()); 
		
		return contacts;
	}
	
	public void addMeeting(View button){
		
		//Czas
		String time="";
		DatePicker datePicker = (DatePicker)findViewById(R.id.KUUUUUR);
		TimePicker timePicker = (TimePicker)findViewById(R.id.pick_meet_time);
		NumberFormat f = DecimalFormat.getInstance();
		f.setMinimumIntegerDigits(2);
		int year = datePicker.getYear();
		int month = datePicker.getMonth()+1;
		int day = datePicker.getDayOfMonth();
		int hour = timePicker.getCurrentHour();
		int minute = timePicker.getCurrentMinute();
		time = ""+year+"-"+f.format(month)+"-"+f.format(day)+" "+f.format(hour)+":"+f.format(minute);
		
		//Klient
		int pos = chooseClient.getSelectedItemPosition();
		int cl_id = posIds.get(pos);
		int contact = chooseContact.getSelectedItemPosition();
		int con = conIds.get(contact);
		
		//Opis
		EditText des = (EditText)findViewById(R.id.description);
		String descr=des.getText().toString();
		
		//Dodaj do bazy
		meetingToBase(cl_id, con, time, descr);
		Toast.makeText(context, "Dodano nowe spotkanie", Toast.LENGTH_SHORT).show();
		Intent mList = new Intent(this, MeetingsList.class);
		startActivity(mList);
		
	}
	
	public void meetingToBase(int cl_id, int contact, String time, String descr){
		//Toast.makeText(context, ""+cl_id+" "+contact+" "+time+" "+descr, Toast.LENGTH_LONG).show();
		ContentValues values = new ContentValues();
		//Toast.makeText(context, "Kontakt: "+contact, Toast.LENGTH_SHORT).show();
		values.put(FeedEntry.MT_CL, cl_id);
		values.put(FeedEntry.MT_CON, contact);
		values.put(FeedEntry.MT_TIME, time);
		values.put(FeedEntry.MT_DESCR, descr);
		db.insert(FeedEntry.MT_TABLE,null,values);
	}
	
	public void addContact(View button){
		int pos = chooseClient.getSelectedItemPosition();
		int cl_id = posIds.get(pos);
        addContact contact = new addContact(context);
        Bundle ids = new Bundle();
        ids.putInt("companyName", cl_id);
        contact.setArguments(ids);
	    contact.show(getFragmentManager(), "tag");
	    updateSpinner();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

	public void updateSpinner(){
		int pos = chooseClient.getSelectedItemPosition();
		int cl_id = posIds.get(pos);
		String client_id = Integer.toString(cl_id);
		List<String> contactsList = loadContacts(client_id);
		
		chooseContact = (Spinner)findViewById(R.id.choose_contact);
		ArrayAdapter<String> con_adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, contactsList);
		chooseContact.setAdapter(con_adapter);
	}

	public void hideKeyboard(View button){
		 
		InputMethodManager imm = (InputMethodManager)getSystemService(
		Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(button.getWindowToken(), 0);
		
	}
}
