package com.example.managerspotkan;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

public class MeetingsList extends ExpandableListActivity {
	
	Context context;
	SQLiteDatabase db;
	ExpandableListView el;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_meetings_list);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		context = getApplicationContext();
		DbHelper mDbHelper = new DbHelper(this);
		db = mDbHelper.getReadableDatabase();
		loadMeetings();

		el =  getExpandableListView();
		
		Cursor c=loadMeetings();		
		
		Adapter adapter=new Adapter(context, c, el);
	    setListAdapter(adapter);

	}
	
	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id){
		Intent meeting = new Intent(this, MeetingView.class);
		meeting.putExtra("meet_id", ""+id);
		startActivity(meeting);
		return false;
	}
	
	public Cursor loadMeetings(){
		String[] projection = {
				FeedEntry.MT_ID,
				FeedEntry.MT_CL, 
				FeedEntry.MT_TIME, 
				FeedEntry.MT_DESCR,
				FeedEntry.MT_ENDTIME, 
				FeedEntry.MT_LOCATION
		};

		String sortOrder = FeedEntry.MT_TIME;
		Cursor c = db.query(
				FeedEntry.MT_TABLE,  // The table to query
			    projection,                               // The columns to return
			    null,                                // The columns for the WHERE clause
			    null,                            // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    sortOrder                                 // The sort order
				);
		
		return c;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
			
}
