package com.example.managerspotkan;

import com.example.managerspotkan.FeedReaderContract.FeedEntry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class EditClient extends Activity {
	
	String id;
	EditText name;
	EditText city;
	EditText code;
	EditText street;
	EditText num;
	
	DbHelper mDbHelper;
	SQLiteDatabase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_client);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		Intent intent = getIntent();
		id = intent.getStringExtra("client_id");
		name = (EditText)findViewById(R.id.edit_client_name);
		city = (EditText)findViewById(R.id.edit_client_city);
		code = (EditText)findViewById(R.id.edit_client_code);
		street = (EditText)findViewById(R.id.edit_client_street);
		num = (EditText)findViewById(R.id.edit_client_num);
		mDbHelper = new DbHelper(this);
		db = mDbHelper.getReadableDatabase();
		loadInfo();
	}
	
	void loadInfo(){
		
		String[] projection = {
				FeedEntry.CL_ID,
				FeedEntry.CL_NAME, 
				FeedEntry.CL_CITY, 
				FeedEntry.CL_CODE, 
				FeedEntry.CL_ST, 
				FeedEntry.CL_NUM
		};

		//String sortOrder = FeedEntry.CL_NAME+" DESC";
		Cursor cursor = db.query(
				FeedEntry.CL_TABLE,  // The table to query
			    projection,  // The columns to return
			    FeedEntry.CL_ID +" like "+id, 	// The columns for the WHERE clause
			    null,  // The values for the WHERE clause
			    null,                                     // don't group the rows
			    null,                                     // don't filter by row groups
			    null                                 // The sort order
				);
		
		cursor.moveToFirst();
        String clientName = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_NAME));
		String clientCity = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_CITY));
		String clientCode = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_CODE));
		String clientStreet = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_ST));
		String clientNumber = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.CL_NUM));
		name.setText(clientName, TextView.BufferType.EDITABLE);
		city.setText(clientCity, TextView.BufferType.EDITABLE);
		code.setText(clientCode, TextView.BufferType.EDITABLE);
		street.setText(clientStreet, TextView.BufferType.EDITABLE);
		num.setText(clientNumber, TextView.BufferType.EDITABLE);
	}
	
	public void hideKeyboard(View button){
		 
		InputMethodManager imm = (InputMethodManager)getSystemService(
		Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(button.getWindowToken(), 0);
		
	}
	
	public void editClient(View button){
		db.execSQL("UPDATE "+FeedEntry.CL_TABLE+" SET "
				+ FeedEntry.CL_NAME + " = '" + name.getText().toString() + "', "
				+ FeedEntry.CL_CITY + " = '" + city.getText().toString() + "', "
				+ FeedEntry.CL_CODE + " = '" + code.getText().toString() + "', "
				+ FeedEntry.CL_ST + " = '" + street.getText().toString() + "', "
				+ FeedEntry.CL_NUM + " = '" + num.getText().toString() + "'"
				+ " WHERE " + FeedEntry.CL_ID + " = " + id);
		Intent back = new Intent(this, ClientView.class);
		back.putExtra("client_id", ""+id);
		startActivity(back);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
